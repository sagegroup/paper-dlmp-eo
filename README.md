# A study of deep learning model performance for remote sensing image segmentation

Team: [SAGEGroup](http://sage.ieat.ro)

Keras (TensorFlow backend) implementation of the following models (improved as described in the paper):

* unet
* wnet
* hsn
* segnet
* rrf_deconvnet

The implementations are using inception or residual block as implemented in [Keras](https://github.com/keras-team/keras) 

Pre-trained weights for the corresponging models: [Click Here](https://drive.google.com/open?id=1_X0JTp_qnUPV-tWoj8mJvzx-T1vSluWM)

Model comparison

![Model Comparison](https://drive.google.com/uc?export=view&id=1-pQDPAfo85rsBXFunzgRMYGyqQjIoLzl)

### Dataset

[Urban3d Challenge Dataset](https://spacenetchallenge.github.io/datasets/Urban_3D_Challenge_summary.html)



## Citation
Please cite
```

```


Building detection example demo with W-Net

![Wnet Demo](https://drive.google.com/uc?export=view&id=1-M0jZn3UnAl4Dth6yQjsXWMP62-HbgPV)
