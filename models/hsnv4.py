from keras import Input, Model
from keras.layers import Conv2D, Activation, concatenate, add, MaxPooling2D, Conv2DTranspose

from tools.utils import custom_objects, f1, dice_coef





def inception(input, filters, activation='elu', padding="same"):

    conv1_1 = Conv2D(filters[0],(1,1), padding=padding)(input)
    conv1_1 = Activation(activation)(conv1_1)
    conv1_2 = Conv2D(filters[1],(3,3), padding=padding)(conv1_1)
    conv1_2 = Activation(activation)(conv1_2)

    conv2_1 = Conv2D(filters[2], (1, 1), padding=padding)(input)
    conv2_1 = Activation(activation)(conv2_1)
    conv2_2 = Conv2D(filters[3], (5, 5), padding=padding)(conv2_1)
    conv2_2 = Activation(activation)(conv2_2)


    conv3_1 = Conv2D(filters[4], (1, 1), padding=padding)(input)
    conv3_1 = Activation(activation)(conv3_1)
    conv3_2 = Conv2D(filters[5], (7, 7), padding=padding)(conv3_1)


    conv4 = Conv2D(filters[6], (1, 1), padding=padding)(input)
    conv4 = Activation(activation)(conv4)


    y = concatenate([conv1_2,conv2_2,conv3_2,conv4])

    return y





def residual (input, activation='elu',padding="same"):

    x = Conv2D(128, (1,1), padding=padding)(input)
    x = Activation(activation)(x)

    x = Conv2D(128, (3,3), padding=padding)(x)

    shortcut = Conv2D(128,(1,1), padding=padding)(input)

    y = add([x, shortcut])
    y = Activation('elu')(y)

    return y

@custom_objects({'f1': f1, 'dice_coef': dice_coef})
def hsnv4(input_width=256,
         input_height=256,
         n_channels=3, nr_classes=2,kernel=3,
        padding="same"):


    img_input = Input(shape=(input_height, input_width, n_channels))

    # A layers
    x = Conv2D(64,(kernel, kernel), padding=padding)(img_input)
    x = Activation('elu')(x)
    x = Conv2D(64, (3, 3), padding=padding)(x)
    x = Activation('elu')(x)
    x = MaxPooling2D((2,2), padding=padding)(x)


    #B layers
    x = Conv2D(128, (kernel, kernel), padding=padding)(x)
    x = Activation('elu')(x)
    x = Conv2D(128, (3, 3), padding=padding)(x)
    x = Activation('elu')(x)

    input_g1 = x
    x = MaxPooling2D((2,2), padding=padding)(x)


    #C layers
    x = inception(x,[128,128,64,32,32,32,64])
    x = inception(x, [128, 128, 64, 32, 32, 32, 64])
    input_g2 = x
    x = MaxPooling2D((2,2), padding=padding)(x)


    #D layers
    x = inception(x, [256, 384, 64, 32, 32, 32, 64])
    x = inception(x, [256, 384, 64, 32, 32, 32, 64])
    x = inception(x, [256, 384, 64, 32, 32, 32, 64])

    #G layers
    g1 = residual (input_g1)
    g2 = residual (input_g2)


    #F layer
    x = Conv2DTranspose(512,(2,2), strides=(2,2))(x)
    x = concatenate([x,g2])
    x = inception(x, [128, 128, 64, 32, 32, 32, 64])
    x = inception(x, [128, 128, 64, 32, 32, 32, 64])
    x = Conv2DTranspose(512,(2,2), strides=(2,2))(x)

    x = concatenate([x,g1])
    x = Conv2D(128, (3, 3), padding=padding)(x)
    x = Activation('elu')(x)

    x = Conv2D(128, (3, 3), padding=padding)(x)
    x = Activation('elu')(x)

    x = Conv2DTranspose(512,(2,2), strides=(2,2))(x)

    x = Conv2D(nr_classes, (1,1))(x)
    x = Activation('softmax')(x)



    # Create model.
    model = Model(inputs=[img_input], outputs=[x], name='hsnv4')

    return model
