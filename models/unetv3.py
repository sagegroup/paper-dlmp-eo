from keras import Input
from keras.engine import Model
from keras.layers import ZeroPadding2D, Conv2D, MaxPooling2D, concatenate, Conv2DTranspose, Cropping2D

from tools.utils import custom_objects, f1, dice_coef





@custom_objects({'f1': f1, 'dice_coef': dice_coef})
def unetv3(input_width=256,
            input_height=256,
            n_channels=3,
            kernel=3,
            stride=1,
            activation='elu',
            nr_classes=2,
            kinit='RandomUniform',
            padding='same',
            axis=3,
            crop=0,
            mpadd=0,
            ):

    img_input = Input((input_height, input_width, n_channels))


    conv1 = ZeroPadding2D((crop, crop))(img_input)
    conv1 = Conv2D(32, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv1)
    conv1 = Conv2D(32, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv1)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(64, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(pool1)
    conv2 = Conv2D(64, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv2)

    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(128, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(pool2)
    conv3 = Conv2D(128, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv3)

    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(256, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(pool3)
    conv4 = Conv2D(256, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv4)

    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

    conv5 = Conv2D(512, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(pool4)
    conv5 = Conv2D(512, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv5)

    up6 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding=padding)(conv5), conv4], axis=axis)
    conv6 = Conv2D(256, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(up6)
    conv6 = Conv2D(256, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv6)

    up7 = concatenate([Conv2DTranspose(128, (2, 2), strides=(2, 2), padding=padding)(conv6), conv3], axis=axis)
    conv7 = Conv2D(128, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(up7)
    conv7 = Conv2D(128, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv7)

    up8 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding=padding)(conv7), conv2], axis=axis)
    conv8 = Conv2D(64, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(up8)
    conv8 = Conv2D(64, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv8)

    up9 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding=padding)(conv8), conv1], axis=axis)
    conv9 = Conv2D(32, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(up9)
    conv9 = Conv2D(32, kernel_size=kernel, strides=stride, activation=activation, kernel_initializer=kinit, padding=padding)(conv9)


    conv9 = Cropping2D((mpadd, mpadd))(conv9)

    conv10 = Conv2D(nr_classes, (1, 1), activation='softmax')(conv9)

    model = Model(inputs=[img_input], outputs=[conv10], name='unetv3')

    return model


