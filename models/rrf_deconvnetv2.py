from keras import Input, Model
from keras.layers import BatchNormalization, Activation, Conv2D, add, concatenate, Conv2DTranspose

from tools.utils import custom_objects, dice_coef, f1





def residual_block(input, output_channel,activation='relu', kernel=3,padding="same"):

    res_1 = BatchNormalization()(input)
    res_1 = Activation(activation)(res_1)
    res_1 = Conv2D(output_channel, (kernel,kernel), strides=(1,1), padding=padding)(res_1)
    res_1 = BatchNormalization()(res_1)
    res_1 = Activation(activation)(res_1)
    res_1 = Conv2D(output_channel, (1,1), strides=(1,1), padding=padding)(res_1)

    shortcut = Conv2D(output_channel, (1, 1),strides=(1, 1))(input)
    shortcut = BatchNormalization()(shortcut)

    res_1 = add([res_1, shortcut])



    res_2 = BatchNormalization()(res_1)
    res_2 = Activation(activation)(res_2)
    res_2 = Conv2D(output_channel, (3,3), strides=(1,1), padding=padding)(res_2)
    res_2 = BatchNormalization()(res_2)
    res_2 = Activation(activation)(res_2)
    res_2 = Conv2D(output_channel, (1,1), strides=(1,1), padding=padding)(res_2)

    res_2 = add([res_2, res_1])

    return res_2

@custom_objects({'f1': f1,'dice_coef': dice_coef})
def rrf_deconvnetv2(input_width=256,
            input_height=256,
            n_channels=3,
            kernel=3,
            stride=1,
            activation='relu',
            nr_classes=2,

            padding='same'):


    img_input = Input((input_height, input_width, n_channels))

    res_1 = residual_block(img_input,32)
    conv_1 = Conv2D(64, (3,3), strides=(2,2), padding=padding)(res_1)

    res_2 = residual_block(conv_1,64)
    conv_2 = Conv2D(128, (3,3), strides=(2,2), padding=padding)(res_2)

    res_3=residual_block(conv_2,128)
    dil_conv_1= Conv2D(256, (3,3), strides=(1,1), dilation_rate=(2,2), padding=padding)(res_3)

    res_4=residual_block(dil_conv_1,256)
    dil_conv_2= Conv2D(256, (3,3), strides=(1,1), dilation_rate=(2,2), padding=padding)(res_4)

    res_5 = residual_block(dil_conv_2,256)
    dil_conv_3 = Conv2D(128, (3,3), strides=(1,1), dilation_rate=(2,2), padding=padding)(res_5)

    concat_1 = concatenate([res_3,dil_conv_3],-1)

    res_6 = residual_block(concat_1,128)
    deconv_1 = Conv2DTranspose(64,(3,3),strides=(2,2),padding=padding)(res_6)

    concat_2 = concatenate([res_2,deconv_1],-1)

    res_7 = residual_block(concat_2,64)
    deconv_2 = Conv2DTranspose(32,(3,3),strides=(2,2),padding=padding)(res_7)
    concat_3 = concatenate([res_1,deconv_2],-1)

    res_8 = residual_block(concat_3,2)
    outputs = Activation('softmax')(res_8)

    model = Model(inputs=[img_input], outputs=[outputs], name='rrf_deconvnetv2')
    return model
