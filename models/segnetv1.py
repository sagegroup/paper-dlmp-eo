from keras import Input, Model
from keras.layers import Conv2D, Activation, BatchNormalization, MaxPooling2D, Dropout, UpSampling2D

from tools.utils import custom_objects, f1, dice_coef





@custom_objects({'f1': f1, 'dice_coef': dice_coef})
def segnetv1(input_width=256,
            input_height=256,
            n_channels=3,
            kernel=3,
            stride=1,
            activation='relu',
            nr_classes=2,
            padding='same', drop=0.5):


    #encoder
    img_input = Input((input_height, input_width, n_channels))

    x = Conv2D(64, (kernel, kernel), padding=padding)(img_input)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(64, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = MaxPooling2D()(x)

    x = Conv2D(128, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(128, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = MaxPooling2D()(x)

    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x= MaxPooling2D()(x)
    x= Dropout(drop)(x)

    x= Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = MaxPooling2D()(x)
    x = Dropout(drop)(x)

    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = MaxPooling2D()(x)
    x = Dropout(drop)(x)

    #decoder

    x = UpSampling2D()(x)

    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Dropout(drop)(x)

    x = UpSampling2D()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(512, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Dropout(drop)(x)

    x = UpSampling2D()(x)
    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(256, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(128, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Dropout(drop)(x)

    x = UpSampling2D()(x)
    x = Conv2D(128, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)
    x = Conv2D(64, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = UpSampling2D()(x)
    x = Conv2D(64, (kernel, kernel), padding=padding)(x)
    x = Activation(activation)(x)
    x = BatchNormalization()(x)

    x = Conv2D(nr_classes, (1, 1), padding='valid')(x)
    x = BatchNormalization()(x)
    x = Activation('softmax')(x)

    model = Model(inputs=[img_input], outputs=[x], name='segnetv1')

    return model